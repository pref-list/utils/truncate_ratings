use std::io;
use std::io::prelude::*;

extern crate itertools;
use itertools::Itertools;

use structopt::StructOpt;

/// Truncates users and items using a threshold in number of ratings.
/// Data is read from the stdin.
#[derive(StructOpt, Debug)]
#[structopt(name = "truncate_ratings")]
struct Args {
    /// Minimum number of ratings for a user
    #[structopt(short, long, default_value = "20")]
    user_trim: usize,

    /// Minimum number of ratings for an item
    #[structopt(short, long, default_value = "500")]
    item_trim: usize
}

#[derive(Debug)]
struct Rating {
    user: String,
    item: usize,
    rating: f32
}

impl Rating {
    pub fn parse_rating(record: String) -> Rating {
        let mut rating = record.split(",");
        let name = rating.next().unwrap().to_string();
        let anime = rating.next().unwrap().parse::<usize>().unwrap();
        let rating_val = rating.next().unwrap().parse::<f32>().unwrap();

        Rating {
            user: name,
            item: anime,
            rating: rating_val
        }
    }

    pub fn to_string(&self) -> String {
        format!("{},{},{}", self.user, self.item, self.rating)
    }
}

fn main() {
    // These will later be arguments
    let args = Args::from_args();
    let user_trim = args.user_trim;
    let item_trim = args.item_trim;

    let reader = io::stdin();

    let mut ratings: Vec<Rating> = reader.lock().lines()
      .map(|l| l.unwrap())
      .filter(|l| l.len() > 1)
      .map(|l| Rating::parse_rating(l))
      .group_by(|e| e.user.clone()).into_iter()
      .map(|(_key, group)| group.collect())
      .filter(|user: &Vec<Rating>| user.len() > user_trim)
      .flatten()
      .collect();

    let mut prev_num_ratings = ratings.len();
    loop {
        eprintln!("Item.. {:}", ratings.len());
        ratings.sort_by_key(|k| k.item.clone());
        ratings = ratings.into_iter()
            .group_by(|e| e.item.clone()).into_iter()
            .map(|(_key, group)| group.collect())
            .filter(|item: &Vec<Rating>| item.len() > item_trim)
            .flatten()
            .collect();

        if ratings.len() == prev_num_ratings {
            break
        }
        prev_num_ratings = ratings.len();

        eprintln!("User.. {:}", ratings.len());
        ratings.sort_by_key(|k| k.user.clone());
        ratings = ratings.into_iter()
            .group_by(|e| e.user.clone()).into_iter()
            .map(|(_key, group)| group.collect())
            .filter(|user: &Vec<Rating>| user.len() > user_trim)
            .flatten()
            .collect();

        if ratings.len() == prev_num_ratings {
            break
        }
        prev_num_ratings = ratings.len();
    };

    eprintln!("{:}", ratings.len());

    ratings.sort_by_key(|k| k.user.clone());
    for r in ratings {
        println!("{}", r.to_string());
    }
}
